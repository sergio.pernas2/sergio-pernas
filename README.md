# Dockerfile

Laboratorio `Dockerfile`.

## Crear imagen

#### Clonar repositorio

```
$ git clone https://gitlab.com/sergio.pernas2/sergio-pernas.git app/
$ cd app/
```

#### Construir imagen

```
$ sudo docker build -t app-image .
```

#### Desplegar contendor


Con comandos docker.

```
$ docker run -d \
    -p 8080:80 \
    --name app-deploy \
    --restart=always \
    app-image
```

Con docker compose.

```
$ sudo docker-compose up -d
```

