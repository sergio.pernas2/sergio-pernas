# imagen con un sistema operativo base

FROM ubuntu


# instalar servidor web dentro del contendor.

RUN apt update && apt install apache2 -y

# Exponer puerto de escucha del contendor

EXPOSE 80

# copiar archivos del directorio html al directorio correspondiente
# dentro del contenedor

COPY html/* /var/www/html


# Comando que se ejecuta cuando se despliega el contendor.

CMD ["apache2ctl", "-D", "FOREGROUND"]


